# As, Not For

As, Not For

Cette page a été créée à l’occasion de la discussion dans l’exposition As, Not For : Détrôner nos absolus” avec Jean-Sylvain Tshilumba Mukendi (écrivain et historien de l'art) — dans le but de créer des liens entre les enjeux présentés dans l’exposition et un contexte plus local (Liège, Belgique) en s’intéressant aux croisements entre création et approches décoloniales.

Typographie: The Neue Black, par Vocal Type Co.

Conçu par Loraine Furter et Jean-Sylvain Tshilumba Mukendi
