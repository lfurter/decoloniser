goLeft = function() {
    $fig = $("figure.big").first();
    $figPrev = ($fig).prev();
    if ($figPrev.length) {
        $fig.toggleClass("big");
        $fig.find("figcaption").toggle();
        $fig.find("figcaption").toggleClass("big-caption");
        $figPrev.toggleClass("big");
        $figPrev.find("figcaption").toggle();
        $figPrev.find("figcaption").toggleClass("big-caption");
    }
};

goRight = function() {
    $fig = $("figure.big").first();
    $figNext = ($fig).next();
    if ($figNext.length) {
        $fig.toggleClass("big");
        $fig.find("figcaption").toggle();
        $fig.find("figcaption").toggleClass("big-caption");
        $figNext.toggleClass("big");
        $figNext.find("figcaption").toggle();
        $figNext.find("figcaption").toggleClass("big-caption");
    }
};

document.onkeydown = function(e) {
    e = e || window.event;
    var charCode = (typeof e.which === "number") ? e.which : e.keyCode;
    console.log(charCode);
    switch(e.which) {
        case 37: // left
            console.log("left!");
            goLeft();
            break;
        case 39: // right
            console.log("right!");
            goRight();
            break;
        default:
            return; // exit this handler for other keys};
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
};
